The logic behind solving Sudoku is described in the attached file [DESCRIPTION](https://gitlab.com/jaweherMansourii/sudokugame/-/blob/master/SudokuLogic.pdf)

Screenshots:

![Image description](./SudokuInterface.PNG)![Image description](./SudokuSolution.PNG)
