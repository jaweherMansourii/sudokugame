﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SudokuTest
{
    public partial class Form1: Form
    {
        
     
        private System.Windows.Forms.Button btnTry;
		private System.Windows.Forms.Button btnReset;
		

		private ComboBox[,] boxes;
		

		public Form1()
		{
			
			InitializeComponent();

			boxes = new ComboBox[9,9];

			for(int x = 0; x < 9; x++)
				for(int y = 0; y < 9; y++)
				{
					ComboBox comboBox = new ComboBox();
					String[] items = new String[]{"-","1","2","3","4","5","6","7","8","9"};
					comboBox.Items.AddRange(items);
					comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
					comboBox.Location = new Point(x*40 + 10,y*30 + 10);
					comboBox.Size = new Size(36, 21);
					comboBox.SelectedIndex = 0;
					comboBox.MaxDropDownItems = 10;
					this.Controls.Add(comboBox);
					boxes[y,x] = comboBox;

				}

			
		
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint (e);
			Pen pen = new Pen(Color.Olive, 3);
			Graphics formGfx = this.CreateGraphics();

			formGfx.DrawRectangle(pen, 6,6,362,267);
			pen.Width = 3;

			Rectangle[] rectangle = new Rectangle[]
			{
				new Rectangle(8,8,119,86), new Rectangle(127,8,119,86), new Rectangle(246,8,121,86),
				new Rectangle(8,94,119,90), new Rectangle(127,94,119,90), new Rectangle(246,94,121,90),
				new Rectangle(8,184,119,88), new Rectangle(127,184,119,88), new Rectangle(246,184,121,88)
			};
			formGfx.DrawRectangles(pen, rectangle);

			pen.Dispose();
			formGfx.Dispose();
		}

		/// <summary>

		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		
		private void InitializeComponent()
		{
            this.btnTry = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnTryDesign
            // 
            this.btnTry.BackColor = System.Drawing.Color.Beige;
            this.btnTry.FlatAppearance.BorderColor = System.Drawing.Color.Olive;
            this.btnTry.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTry.Location = new System.Drawing.Point(420, 44);
            this.btnTry.Name = "btnTry";
            this.btnTry.Size = new System.Drawing.Size(75, 23);
            this.btnTry.TabIndex = 0;
            this.btnTry.Text = "Try";
            this.btnTry.UseVisualStyleBackColor = false;
            this.btnTry.Click += new System.EventHandler(this.btnTry_Click);
            // 
            // btnResetDesign
            // 
            this.btnReset.BackColor = System.Drawing.Color.Beige;
            this.btnReset.FlatAppearance.BorderColor = System.Drawing.Color.Olive;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Location = new System.Drawing.Point(420, 87);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 3;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(564, 349);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnTry);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sudoku Game";
            this.Load += new System.EventHandler(this.Game_Load);
            this.ResumeLayout(false);

		}
		#endregion

		private byte[,] GetData()
		{
			byte[,] d = new byte[9,9];
			for(int y = 0; y < 9; y++)
				for(int x = 0; x < 9; x++)
					d[y,x] = (byte)boxes[y,x].SelectedIndex;

			return d;
		}

		private void SetData(byte[,] d)
		{
			for(int y = 0; y < 9; y++)
				for(int x = 0; x < 9; x++)
					boxes[y,x].SelectedIndex = d[y,x];
		}

		// Solve
		private void btnTry_Click(object sender, System.EventArgs e)
		{
			Sudoku s = new Sudoku();
			byte[,] d = GetData();
			s.Data = d;

			
			if(!s.IsSudokuFeasible())
			{
                DialogResult result = MessageBox.Show("invalid choice, reset all and retry ? ", "ERROR!", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    s.Clear();
                    for (int y = 0; y < 9; y++)
                        for (int x = 0; x < 9; x++)
                            boxes[y, x].SelectedIndex = 0;
                }
                else if (result == DialogResult.No)
                {
                   
                }            
                return;
			}

			

			if(s.Solve())
			{
				//  valid choice 


				d = s.Data;
				SetData(d);
			}
			else
			{

				//  failed solution

			}



		}

		
	

		// Reset square
		private void btnReset_Click(object sender, System.EventArgs e)
		{
			for(int y = 0; y < 9; y++)
				for(int x = 0; x < 9; x++)
					boxes[y,x].SelectedIndex = 0;
		}

		

	



		/// <summary>
		/// The main entry point for the application.
		/// </summary>
	
	

        private void Game_Load(object sender, EventArgs e)
        {

        }
    }
    }
